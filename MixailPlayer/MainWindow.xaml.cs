﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MixailPlayer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Dictionary<Button, int> MusicList;
        private int currentTrack;
        private int count;
        private MediaPlayer player;
        private List<string> paths;

        private void playSound(string path)
        {
            player.MediaFailed += (s, e1) => MessageBox.Show(e1.ToString());
            player.Open(new Uri(path, UriKind.RelativeOrAbsolute));
            player.Play();
        }

        public MainWindow()
        {
            InitializeComponent();
            player = new MediaPlayer();
            MusicList = new Dictionary<Button, int>();
            paths = new List<string>();
            currentTrack = 0;
            count = 0;
            volumeSlider.Value = 50;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Audio Files (.mp3)|*.mp3";
            if (dialog.ShowDialog() == true)
            {
                string path = dialog.FileName;
                playSound(path);
                playList.Children.Add(new Button());
                var cash = (Button)playList.Children[playList.Children.Count - 1];
                MusicList.Add(cash, count++);
                paths.Add(dialog.FileName);
                cash.Content = dialog.SafeFileName;
                cash.Click += (s1, e1) =>
                {
                    playSound(paths[MusicList[cash]]);
                    currentTrack = MusicList[cash];
                };
            }
        }

        private void playButton_Click(object sender, RoutedEventArgs e)
        {
            player.Play();
        }

        private void pauseButton_Click(object sender, RoutedEventArgs e)
        {
            player.Pause();
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            player.Stop();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            player.Pause();
            if (volumeSlider.Value!=0)   player.Volume = volumeSlider.Value/100.0;
            else player.Volume = volumeSlider.Value;
            player.Play();
<<<<<<< HEAD
          //  MessageBox.Show("TryChange");
        }

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            if (currentTrack > 0) playSound(paths[--currentTrack]);
            else
            {
                currentTrack = paths.Count-1;
                playSound(paths[currentTrack]);
            }
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            if (currentTrack < paths.Count-1) playSound(paths[++currentTrack]);
            else
            {
                currentTrack = 0;
                playSound(paths[currentTrack]);
            }

=======
>>>>>>> 29ed99075059dcaaebc40c02876cabacb405688c
        }
    }
}
